<!DOCTYPE html>
<html>
<head>
	<title>dev</title>  
		<link rel="stylesheet" href="jquery-ui.css">
        <link rel="stylesheet" href="bootstrap.min.css" />
		<script src="jquery.min.js"></script>  
		<script src="jquery-ui.js"></script>
</head>
<body>
	<body background="m01.jpg">
	<div class="container">
			<br />
			<h3 align="center"><font color="white">Dev</h3></font><br />
			<br />
				<div align="right" style="margin-bottom:5px;">
					<button type="button" name="add" id="add" class="btn btn-success btn-xs">Add</button>
				</div>
				<div class="table-responsive" id="user_data"></div>
			<br/>
		</div>
		
		<?php //form add-dev ?>
		<div id="user_dialog" title="Add dev">
			<form method="post" id="user_form">
				
			    <div class="form-group">
					<label>dev Id</label>
					<input type="text" name="dev_id" id="dev_id" class="form-control" />
					<span id="error_dev_id" class="text-danger"></span>
				</div>

				<div class="form-group">
					<label>dev Name</label>
					<input type="text" name="dev_name" id="dev_name" class="form-control" />
					<span id="error_request_name" class="text-danger"></span>
				</div>


				<div class="form-group">
					<input type="hidden" name="action" id="action" value="insert" />
					<input type="hidden" name="hidden_id" id="hidden_id" />
					<input type="submit" name="form_action" id="form_action" class="btn btn-info" value="Insert" />
				</div>
			</form>
		</div>
		
</body>
</html>

	<script>  
$(document).ready(function(){  

	load_data();
    
	function load_data()
	{
		$.ajax({
			url:"fetch.php",
			method:"POST",
			success:function(data)
			{
				$('#user_data').html(data);
			}
		});
	}
	
	$("#user_dialog").dialog({
		autoOpen:false,
		width:400
	});
	
	$('#add').click(function(){
		$('#user_dialog').attr('title', 'Add Data');
		$('#action').val('insert');
		$('#form_action').val('Insert');
		$('#user_form')[0].reset();
		$('#form_action').attr('disabled', false);
		$("#user_dialog").dialog('open');
	});
	
	$('#user_form').on('submit', function(event){
		event.preventDefault();
		var error_dev_id = '';
		var error_dev_name = '';
	
		
		if($('#dev_id').val() == '')
		{
			error_dev_id = 'dev_id is required';
			$('#error_dev_id').text(error_dev_id);
			$('#dev_id').css('border-color', '#cc0000');
		}
		else
		{
			error_dev_id = '';
			$('#error_dev_id').text(error_dev_id);
			$('#dev_id').css('border-color', '');
		}
		
		if($('#dev_name').val() == '')
		{
			error_dev_name = 'dev_name is required';
			$('#error_dev_name').text(error_dev_name);
			$('#dev_name').css('border-color', '#cc0000');
		}
		else
		{
			error_dev_name = '';
			$('#error_dev_name').text(error_dev_name);
			$('#dev_name').css('border-color', '');
		}

		

		if(error_dev_id != '' || error_dev_name != '')
		{
			return false;
		}
		else
		{
			$('#form_action').attr('disabled', 'disabled');
			var form_data = $(this).serialize();
			$.ajax({
				url:"action.php",
				method:"POST",
				data:form_data,
				success:function(data)
				{
					$('#user_dialog').dialog('close');
					$('#action_alert').html(data);
					$('#action_alert').dialog('open');
					load_data();
					$('#form_action').attr('disabled', false);
				}
			});
		}
		
	});
	
	$('#action_alert').dialog({
		autoOpen:false
	});
	
	
	
});  
</script>